package com.paymaya.dbi.maynilad.dao;

import com.google.gson.Gson;
import lombok.Data;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Data
public class PostRequest {

    private String id;
    private Biller biller;
    private Transaction transaction;
    private String status;
    private Result result;
    private Callback callback;

    public PostRequest() {
    }

    public PostRequest(String jsonString) {
        PostRequest pq = new Gson().fromJson(jsonString, PostRequest.class);
        this.id = pq.getId();
        this.biller = pq.getBiller();
        this.transaction = pq.getTransaction();
        this.status = pq.getStatus();
        this.callback = pq.getCallback();
    }

    public PaymentFileDetail getDetails() {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        PaymentFileDetail pfd = new PaymentFileDetail();
        pfd.setCheckNumber("             ");
        pfd.setContractAccountNumber(this.getBiller().getAccountNumber());
        pfd.setDocumentDate(dateFormat.format(date));
        pfd.setPostingDate(dateFormat.format(date));
        pfd.setPaymentAmount(new DecimalFormat(".00").format(this.getTransaction().getAmount().getTotal().getValue()));
        return pfd;
    }

    public String getJsonString() {
        return new Gson().toJson(this, PostRequest.class);
    }
}
