package com.paymaya.dbi.maynilad.dao;

import com.paymaya.dbi.maynilad.helper.DBIConstant;

import java.text.DecimalFormat;

/**
 * Payment File Header
 * This generate and format the Payment file's
 * header
 *
 * @author Mark Lawrence Saballe
 * @since 2018-07-24
 */

public class PaymentFileHeader { //27

    //private char categoryCode = '1';        // 1|1
    //private String tableName = "BFKKZK";    // 30|2
    // others                               // 64
    //private String bankClearingAccount = "9100700000"; // 10|96
    //private String companyCode = "MWSI";    // 4|106
    // others                               // 6
    //private String currencyKey = "PHP";     // 5|116
    // others                               // 10
    private String postingDate;             // 8|131
    // 8|139
    private String documentDate;
    // 9
    //private String clearingReason = "01";   // 2|156
    // others                               // 3
    private String additionalInfo;          // 50|161
    // others                               // 15
    private String creditTotal;             // 15|226
    private String numberOfItems;           // 6|241
    // others                               // 4
    public String entry() {
        String format = "%1s%-30s%64s%10s%4s%6s%-5s%10s%8s%8s%9s%2s%3s%-50s%15s%14s%7s%4s";
        return String
                .format(
                        format,
                        DBIConstant.CATEGORY_CODE_HEADER,
                        DBIConstant.TABLE_NAME_HEADER,
                        "",
                        DBIConstant.BANK_CLEARING_ACCOUNT,
                        DBIConstant.COMPANY_CODE,
                        "",
                        DBIConstant.CURRENCY_KEY,
                        "",
                        this.postingDate,
                        this.documentDate,
                        "",
                        DBIConstant.CLEARING_REASON,
                        "",
                        this.additionalInfo,
                        "",
                        new DecimalFormat(".00").format(Double.valueOf(this.creditTotal)),
                        String.format("%06d", Integer.valueOf(this.numberOfItems)), "");
    }

    public void setPostingDate(String postingDate) {
        this.postingDate = postingDate;
    }

    public void setDocumentDate(String documentDate) {
        this.documentDate = documentDate;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public void setCreditTotal(String creditTotal) {
        this.creditTotal = creditTotal;
    }

    public void setNumberOfItems(String numberOfItems) {
        this.numberOfItems = numberOfItems;
    }
}
