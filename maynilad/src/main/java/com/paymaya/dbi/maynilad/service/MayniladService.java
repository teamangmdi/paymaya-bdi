package com.paymaya.dbi.maynilad.service;

import com.google.gson.Gson;
import com.paymaya.dbi.maynilad.api.Services;
import com.paymaya.dbi.maynilad.dao.*;
import com.paymaya.dbi.maynilad.helper.DBIConstant;
import com.paymaya.dbi.maynilad.helper.Message;
import com.paymaya.dbi.maynilad.helper.ProcessCode;
import com.paymaya.dbi.maynilad.repo.MayniladRepository;
import org.joda.time.DateTime;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Maynilad Services for PayMaya BDI
 * Implements the Services that Validates,
 * Post and Generate
 *
 * @author Mark Lawrence Saballe
 * @since 6/25/2018
 */

@Service
public class MayniladService implements Services {

    private static final Logger LOGGER = LoggerFactory.getLogger(MayniladService.class);
    private Gson gson = new Gson();

    @Autowired
    private MayniladRepository repo;
    @Autowired
    private ReportGeneratorService reportService;
    @Autowired
    private S3Service s3Service;
    @Autowired
    private CallbackService callbackService;
    @Autowired
    private UpdateFlagService updateFlagService;

    /**
     * Method for validation of account number and
     * reorganizing the proper response
     *
     * @param payloadString This is the request which comes form other services
     * @return responseHandler For the response needed whether successful or not
     */
    @Override
    public DBIResponse validate(ValidateRequest payloadString){
        Result result = new Result(validateCAN(payloadString.getBiller().getAccountNumber().toString(), ProcessCode.VALIDATE).getCode());
        ValidateResponse validateResponse = new ValidateResponse();
        try {
            if (result.getCode().equals(Message.SUCCESSFUL.getCode())) {
                BillerValidate billerValidate = new BillerValidate();
                TransactionValidate transactionValidate = new TransactionValidate();
                AmountBreakdown amountBreakdown = new AmountBreakdown();

                BaseAmount baseAmount = new BaseAmount();
                baseAmount.setCurrency(DBIConstant.CURRENCY_KEY);
                baseAmount.setValue(payloadString.getTransaction().getAmount().getValue());

                FeeAmount feeAmount = new FeeAmount();
                feeAmount.setCurrency(DBIConstant.CURRENCY_KEY);
                feeAmount.setValue(DBIConstant.feeValue);

                TotalAmount totalAmount = new TotalAmount();
                totalAmount.setCurrency(DBIConstant.CURRENCY_KEY);
                totalAmount.setValue(DBIConstant.feeValue.add(payloadString.getTransaction().getAmount().getValue()));

                amountBreakdown.setBase(baseAmount);
                amountBreakdown.setFee(feeAmount);
                amountBreakdown.setTotal(totalAmount);

                transactionValidate.setDate(payloadString.getTransaction().getDate());
                transactionValidate.setAmount(amountBreakdown);

                Details details = new Details();
                details.setRAP(payloadString.getBiller().getFields().getRAP());
                details.setSlug(payloadString.getBiller().getSlug());

                billerValidate.setAccountNumber(payloadString.getBiller().getAccountNumber());
                billerValidate.setDetails(details);

                validateResponse.setId(payloadString.getId());
                validateResponse.setResult(result);
                validateResponse.setBiller(billerValidate);
                validateResponse.setTransaction(transactionValidate);

            } else {
                validateResponse.setResult(result);
            }
            return responseHandler(new JSONObject(gson.toJson(validateResponse)));
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage());
            return responseHandler(Message.INTERNAL_ERROR);
        }

    }

    /**
     * Method for posting the transaction after validation
     *
     * @param payload This are the details to be insert in DynamoDB
     * @return responseHandler for the response needed whether successful or not
     */
    @Override
    public DBIResponse postTransaction(PostRequest payload) {
        Message message = validateCAN(payload.getBiller().getAccountNumber(), ProcessCode.POST);
        if (Message.SUCCESSFUL.equals(message)) {
            payload.setStatus("PENDING");
            message = repo.put(payload);
        }
        return responseHandler(message);
    }

    /**
     * Method for generation of payment file
     * Call the call back response and s3 transfer of file
     *
     */
    @Override
    public DBIResponse generate(DateTime date){
        List<PaymentFileDetail> pfds = new ArrayList<>();
        BigDecimal totalAmount = BigDecimal.ZERO;
        List<PostRequest> prs = repo.get(date);
        for (PostRequest trx : prs) {
            totalAmount = totalAmount.add(trx.getTransaction().getAmount().getTotal().getValue());
            pfds.add(trx.getDetails());
        }
            if ( prs.isEmpty() ){
                throw new RuntimeException("No data found");
            } else {
                String generationMessage = reportService.generateMayniladPaymentFile(pfds, totalAmount, date);
                if (Message.INTERNAL_ERROR.getFullMessage().equals(generationMessage)) {
                    return responseHandler(Message.INTERNAL_ERROR);
                } else {
                    if (Message.INTERNAL_ERROR.equals(s3Service.push(generationMessage))) {
                        return responseHandler(Message.INTERNAL_ERROR);
                    } else {
                        updateFlagService.updateFlag(prs);
                        return responseHandler(callbackService.send(prs));
                    }
                }
            }
    }

    /**
     * Response Handler setting the body of response into JSON
     * format
     *
     * @param payload The details needed for the response
     * @return response
     */
    private DBIResponse responseHandler(Object payload) {
        DBIResponse response = new DBIResponse();
        if (payload instanceof JSONObject){
            JSONObject obj = (JSONObject) payload;
            response.setBody(payload.toString());
            if (obj.optJSONObject("result").get("code").toString().equalsIgnoreCase(Message.INVALID_ACCOUNT_NUMBER.getCode())){
                response.setStatusCode(HttpStatus.BAD_REQUEST.toString());
            }
        }
        else {
            response.setBody(gson.toJson(payload));
        }
        return response;
    }

    /**
     * Validation of account number with process
     * code
     *
     * @param can This is the account number to be validated
     * @param transaction This is the process code whether valid or not
     * @return Message result
     */
    private Message validateCAN(String can, ProcessCode transaction) {
        Message result = validateProcess(can);
        //LOGGER.info("TRANSACTION: " + transaction.getFullMessage() + " # ACCOUNT NUMBER: " + can + " # RESULT: " + result);
        return result;
    }

    /**
     * The actual validation of the account number
     *
     * @param can This is the account number to be validated
     * @return String Message if valid or not
     */
    private Message validateProcess(String can) {
        int sum = 0 , n;
        if (can.length() != DBIConstant.LENGTH) {
            return Message.INVALID_LENGTH;
        } else {
            for (int counter = DBIConstant.START; counter < DBIConstant.LENGTH; counter++) {
                int currentCharacter = Character.getNumericValue(can.charAt(counter));
                if (counter == 0) {
                    sum = currentCharacter;
                } else if (counter == DBIConstant.FIRST_SEVEN) {
                    int a = ((sum / DBIConstant.HIGHEST_MULTIPLE) * DBIConstant.HIGHEST_MULTIPLE);
                    if (a != sum) {
                        a += DBIConstant.HIGHEST_MULTIPLE;
                    }
                    if (a - sum == currentCharacter) {
                        return Message.SUCCESSFUL;
                    }
                } else {
                    if (counter % DBIConstant.MULTIPLIER == DBIConstant.ONE) {
                        currentCharacter *= DBIConstant.MULTIPLIER;
                        if ((int) (Math.log10(currentCharacter) + DBIConstant.ONE) == DBIConstant.MULTIPLIER) {
                            n = 0;
                            while (currentCharacter > 0) {
                                n += currentCharacter % DBIConstant.HIGHEST_MULTIPLE;
                                currentCharacter /= DBIConstant.HIGHEST_MULTIPLE;
                            }
                            sum += n;
                        }
                    }
                    sum += currentCharacter;
                }
            }
            return Message.INVALID_ACCOUNT_NUMBER;
        }
    }

}