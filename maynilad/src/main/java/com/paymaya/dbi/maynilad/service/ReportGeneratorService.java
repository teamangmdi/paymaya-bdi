package com.paymaya.dbi.maynilad.service;

import com.google.gson.Gson;
import com.paymaya.dbi.maynilad.dao.PaymentFileDetail;
import com.paymaya.dbi.maynilad.dao.PaymentFileHeader;
import com.paymaya.dbi.maynilad.helper.Message;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Report Generate Service
 * Service that generates the actual payment
 * file
 *
 * @author Mark Lawrence Saballe
 * @since 7/25/2018
 */

@Service
public class ReportGeneratorService {

    @Value("${payment.channel.code}")
    private String paymentChannelCode;

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportGeneratorService.class);
    private Gson gson = new Gson();
    private DateFormat fileFormatter = new SimpleDateFormat("MMddyyyy");
    private DateFormat headerFormatter = new SimpleDateFormat("yyyyMMdd");
    private String localDirectory;
    private Path dirPath;

    public ReportGeneratorService() {
        if (System.getProperty("os.name").startsWith("Windows")) {
            localDirectory = "C:\\tmp\\";
        } else {
            localDirectory = "/tmp/";
        }
    }

    /**
     * The method of generating payment file
     * including the header, details and filename
     *
     * @param pfds This is the payment file details
     * @param totalAmount This is the totalAmount amount of all the transaction made
     * @return fileDir This is the location of the generated payment file
     */

    public String generateMayniladPaymentFile(List<PaymentFileDetail> pfds, BigDecimal totalAmount, DateTime date) {
        try {
            //Generate Payment File's filename
            String filename = paymentChannelCode + "_" + fileFormatter.format(date.toDate()) + ".TXT";

            //Generate PaymentFileHeader
            PaymentFileHeader pfh = new PaymentFileHeader();
            pfh.setAdditionalInfo(filename);
            pfh.setCreditTotal(totalAmount.toString());
            pfh.setDocumentDate(headerFormatter.format(date.toDate()));
            pfh.setNumberOfItems(pfds.size() + "");
            pfh.setPostingDate(headerFormatter.format(date.toDate()));

            //Generate File Directory
            dirPath = checkDirectories(localDirectory);

            String fileDir = dirPath + "/" + filename;
            System.out.println("File Directory: " + fileDir);
            //Generate Payment File
            try (PrintWriter writer = new PrintWriter(new File(fileDir))) {
                writer.printf(pfh.entry());
                writer.print("\r\n");
                for (PaymentFileDetail d : pfds) {
                    writer.printf(d.entry());
                    writer.print("\r\n");
                }
            }
            LOGGER.info("Payment File successfully generated. {}", filename);
            return fileDir;
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error(e.toString());
            return Message.INTERNAL_ERROR.getFullMessage();
        }
    }

    /**
     * Checking if the default directory exists
     * and create if does not
     *
     * @param directory The directory path of the payment file
     * @return Path The location where the payment file should be place
     * @throws Exception
     */
    public Path checkDirectories(String directory) throws Exception {
        Path initialPath = Paths.get(directory);
        if (!Files.exists(initialPath))
            return Files.createDirectories(initialPath);
        else
            return initialPath;
    }
}