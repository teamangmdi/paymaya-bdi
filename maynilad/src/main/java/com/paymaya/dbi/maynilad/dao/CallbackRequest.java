package com.paymaya.dbi.maynilad.dao;

import lombok.Data;

import java.io.Serializable;

@Data
public class CallbackRequest implements Serializable{

    private String id;
    private Biller biller;
    private TransactionCallback transaction;
    private Result result;

}
