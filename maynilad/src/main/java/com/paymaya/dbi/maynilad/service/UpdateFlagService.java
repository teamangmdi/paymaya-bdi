package com.paymaya.dbi.maynilad.service;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.amazonaws.services.dynamodbv2.xspec.L;
import com.paymaya.dbi.maynilad.dao.PostRequest;
import com.paymaya.dbi.maynilad.helper.Message;
import com.paymaya.dbi.maynilad.repo.MayniladRepository;
import javafx.geometry.Pos;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class UpdateFlagService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateFlagService.class);
    private static final String GENERATED = "1";

    @Value("${dynamodb.tablename.dev}")
    private String tableName;

    @Autowired
    private AmazonDynamoDB amazonDynamoDB;
    private DynamoDB dynamoDB;

    @PostConstruct
    public void init() {
        dynamoDB = new DynamoDB(amazonDynamoDB);
    }

    public void updateFlag(List<PostRequest> generatedData){
        Table table = dynamoDB.getTable(tableName);
        List<PostRequest> prs = generatedData;
        for (PostRequest ps : prs ){
            UpdateItemSpec updateItemSpec = new UpdateItemSpec()
                    .withPrimaryKey("Id",ps.getId())
                    .withUpdateExpression("set flag=:flag")
                    .withValueMap(new ValueMap().withString(":flag", GENERATED))
                    .withReturnValues(ReturnValue.UPDATED_NEW);
            try {
                table.updateItem(updateItemSpec);
                LOGGER.info("UpdateItem succeeded!");
            }
            catch (Exception e) {
                LOGGER.error("Unable to update item");
                LOGGER.error(e.getMessage());
            }
        }
    }
}
