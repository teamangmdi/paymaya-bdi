package com.paymaya.dbi.maynilad.repo;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.*;
import com.amazonaws.services.dynamodbv2.xspec.S;
import com.paymaya.dbi.maynilad.config.DBIAccessConfig;
import com.paymaya.dbi.maynilad.dao.PostRequest;
import com.paymaya.dbi.maynilad.helper.Message;
import com.paymaya.dbi.maynilad.service.UpdateFlagService;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Maynilad Repository
 * This class is actual adding and retrieving
 * data into DynamoDB
 *
 * @author Mark Lawrence Saballe
 * @since 9/20/2018
 */

@Component
public class MayniladRepository {
    private static final Logger LOGGER = LoggerFactory.getLogger(MayniladRepository.class);
    private static final String NOT_GENERATED = "0";
    private static final String ID = "Id";
    private static final String DATA = "data";
    private static final String DATE_AND_TIME = "date_and_time";
    private static final String FLAG = "flag";

    @Value("${dynamodb.tablename.dev}")
    private String tableName;

    @Autowired
    private AmazonDynamoDB amazonDynamoDB;
    private DynamoDB dynamoDB;

    @Autowired
    private UpdateFlagService updateFlagService;

    @PostConstruct
    public void init() {
        dynamoDB = new DynamoDB(amazonDynamoDB);
    }

    /**
     * The method for adding the payload date into
     * DynamoDB
     *
     * @param payload The data to be inserted into DynamoDB
     * @return String Message if Successful or not
     */
    public Message put(PostRequest payload) {
        Table table = dynamoDB.getTable(tableName);
        DateTime date = new DateTime();
        try {
            Item item = new Item().withPrimaryKey(ID, payload.getId())
                    .withString(DATA, payload.getJsonString())
                    .withString(DATE_AND_TIME, date.toString())
                    .withString(FLAG, NOT_GENERATED);
            table.putItem(item);
            LOGGER.info("The payload has been successfully saved");
            return Message.SUCCESSFUL;
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage());
            return Message.INTERNAL_ERROR;
        }
    }
    /**
     * The method for retrieving data from
     * DynamoDB and will be use for the generation of
     * payment file
     *
     * @return List of payload data
     */
    public List<PostRequest> get(DateTime eventDate) {
        Table table = dynamoDB.getTable(tableName);
        ItemCollection<ScanOutcome> scan = table.scan();
        List<PostRequest> prs = new ArrayList<>();
        for (Item d : scan) {
            if (d.get(FLAG).toString().equalsIgnoreCase(NOT_GENERATED) &&
                    (eventDate.toString().split("T")[0].equalsIgnoreCase(d.get(DATE_AND_TIME).toString().split("T")[0]))) {
                prs.add(new PostRequest(d.get(DATA).toString()));
            }
        }
        return prs;
    }
}