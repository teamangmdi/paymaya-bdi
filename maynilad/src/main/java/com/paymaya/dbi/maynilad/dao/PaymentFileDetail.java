package com.paymaya.dbi.maynilad.dao;

import com.paymaya.dbi.maynilad.helper.DBIConstant;

/**
 * Payment File Detail
 * This generate and format the Payment file's
 * details
 *
 * @author Mark Lawrence Saballe
 * @since 7/24/2018
 */
public class PaymentFileDetail {

    //private String categoryCode = "2";        //1|1
    //private String tableName = "BFKKZP";    //30|2
    //private char selectionCategory = 'K';   //1|32
    //private String other1;                            //2|33 to 34
    private String contractAccountNumber;   //30|35
    //private String other2;                            //60|65 to 124
    private String paymentAmount;           //15|125
    //private String other3;                            //33|140 to 172
    //private String bankClearingAccount = "9100700000"; //10|173
    //private String companyCode = "MWSI";    //4|183
    //private String other4;                            //6|187 to 192
    //private String currencyKey = "PHP";     //5|193
    //private String other5;                            //10|198 to 207
    private String postingDate;             //8|208
    private String documentDate;            //8|216
    //private String other6;                            //9|216 to 232
    //private String clearingReason = "01";   //2|233
    //private String other7;                            //92|235 to 326
    private String checkNumber;             //13|327
    //private String others;                            //233|327 to 567


    public String entry() {
        String format = "%1s%-30s%1s%2s%-30s%60s%15s%33s%10s%4s%6s%-5s%10s%8s%8s%9s%2s%92s%13s%233s";
        return String.format(format, DBIConstant.CATEGORY_CODE_DETAIL, DBIConstant.TABLE_NAME_DETAIL, DBIConstant.SELECTION_CATEGORY, "",
                this.contractAccountNumber,
                "", this.paymentAmount, "", DBIConstant.BANK_CLEARING_ACCOUNT, DBIConstant.COMPANY_CODE,
                "", DBIConstant.CURRENCY_KEY, "", this.postingDate, this.documentDate,
                "", DBIConstant.CLEARING_REASON, "", this.checkNumber, "");
    }

    public void setContractAccountNumber(String contractAccountNumber) {
        this.contractAccountNumber = contractAccountNumber;
    }

    public void setPaymentAmount(String paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public void setPostingDate(String postingDate) {
        this.postingDate = postingDate;
    }

    public void setDocumentDate(String documentDate) {
        this.documentDate = documentDate;
    }

    public void setCheckNumber(String checkNumber) {
        this.checkNumber = checkNumber;
    }
}
