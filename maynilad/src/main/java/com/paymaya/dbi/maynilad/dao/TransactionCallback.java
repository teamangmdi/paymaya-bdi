package com.paymaya.dbi.maynilad.dao;

import lombok.Data;

@Data
public class TransactionCallback {

    private String receiptNumber;
    private AmountBreakdown amount;
    private String date;
    private String otherInfo;

}
