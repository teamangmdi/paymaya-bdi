package com.paymaya.dbi.maynilad.dao;

import lombok.Data;

@Data
public class AmountBreakdown extends UnitValue {

    private TotalAmount total;
    private FeeAmount fee;
    private BaseAmount base;

}