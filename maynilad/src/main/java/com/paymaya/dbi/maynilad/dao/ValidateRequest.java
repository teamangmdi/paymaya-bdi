package com.paymaya.dbi.maynilad.dao;

import com.google.gson.Gson;
import lombok.Data;

import java.io.Serializable;

@Data
public class ValidateRequest implements Serializable {

    private String id;
    private BillerValidate biller;
    private TransactionValidate transaction;
    private Result result;

    public ValidateRequest() {
    }

    public ValidateRequest(String jsonString) {
        ValidateRequest pq = new Gson().fromJson(jsonString, ValidateRequest.class);
        this.id = pq.getId();
        this.biller = pq.getBiller();
        this.transaction = pq.getTransaction();
        this.result = pq.getResult();
    }

    public String getJsonString() {
        return new Gson().toJson(this, ValidateRequest.class);
    }
}
