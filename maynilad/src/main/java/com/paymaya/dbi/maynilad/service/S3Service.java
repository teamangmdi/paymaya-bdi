package com.paymaya.dbi.maynilad.service;

import com.amazonaws.services.s3.AmazonS3;
import com.paymaya.dbi.maynilad.helper.DBIConstant;
import com.paymaya.dbi.maynilad.helper.Message;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;

/**
 * The service that transfer the generated file
 * into amazon s3
 *
 * @author dondon.martillano
 * @since 9/26/1018
 */

@Service
public class S3Service {
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(S3Service.class);
    private int splitArray = 2;

    @Value("${bucket.name}")
    private String bucketName;

    @Autowired
    private AmazonS3 s3client;

    /**
     * Pushing the generated file into the specific
     * bucket in s3
     *
     * @param fileDirectory The location of the generated file
     * @return String Message whether the transfer is successful or not
     */
    public Message push(String fileDirectory) {
        File temp = new File(fileDirectory);
        try {
            //Bucket Name, Remote, file
            String remote = fileDirectory.split("/")[splitArray]; // if local use 1 else 2
            LOGGER.info(fileDirectory + " : " + remote);
            s3client.putObject(bucketName, remote, temp);
            LOGGER.info("The file has been successfully uploaded");
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage());
            return Message.INTERNAL_ERROR;
        } finally {
            try {
                temp.deleteOnExit();
                LOGGER.info("The file has been successfully deleted");
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage());
            }
        }
        return Message.SUCCESSFUL;
    }
}