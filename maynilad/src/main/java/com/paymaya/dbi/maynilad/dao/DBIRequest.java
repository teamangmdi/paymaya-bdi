package com.paymaya.dbi.maynilad.dao;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * Billers Direct Integration Request
 *
 * @author Mark Lawrence Saballe
 * @since 7/24/2018
 */

@Data
public class DBIRequest implements Serializable {

    private Map<String, String> header;
    private String body;
    private String status;
    private String path;
}
