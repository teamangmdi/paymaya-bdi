package com.paymaya.dbi.maynilad.service;

import com.google.gson.Gson;
import com.paymaya.dbi.maynilad.dao.*;
import com.paymaya.dbi.maynilad.helper.DBIConstant;
import com.paymaya.dbi.maynilad.helper.Message;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.Arrays;
import java.util.List;

/**
 * Callback Service
 * Returning a callback response to billspay service
 * for the successful transaction
 *
 * @author Mark Lawrence Saballe
 * @since 9/20/2018
 */


@Service
public class CallbackService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CallbackService.class);
    private static final int CONNECTION_TIMEOUT = 60000;
    private int splitArray = 4;
    private Gson gson = new Gson();

    @Autowired
    private DecryptionService decryptionService;

    @Value("${callback.request.reference}")
    private String rra;

    @Value("${callback.authorization.first}")
    private String authOne;

    @Value("${callback.authorization.second}")
    private String authTwo;

    @Value("${callback.uri.dev}")
    private String uri;

    @Value("${proxy.host}")
    private String proxyHost;

    @Value("${proxy.port}")
    private int proxyPort;

    @Value("${enableproxy}")
    private boolean enableProxy;

    /**
     * The method invoking the callback response
     * for the proper status update
     *
     * @param trxs This is the List of the details of the request
     * @return String Message if successful or not
     */
    public Message send(List<PostRequest> trxs){
        try {
            for (PostRequest pr : trxs) {
                SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
                if (enableProxy){
                    Proxy proxy = new Proxy(java.net.Proxy.Type.HTTP, new InetSocketAddress(proxyHost, proxyPort));
                    requestFactory.setProxy(proxy);
                }
                requestFactory.setConnectTimeout(CONNECTION_TIMEOUT);

                RestTemplate restTemplate = new RestTemplate(requestFactory);
                HttpHeaders headers = new HttpHeaders();
                headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
                headers.add("Request-Reference-No", rra);
                headers.add("Authorization", decryptionService.decrypt(authOne) + decryptionService.decrypt(authTwo));

                Result result = new Result(Message.SUCCESSFUL.getCode());

                Biller biller = new Biller();
                biller.setAccountNumber(pr.getBiller().getAccountNumber());

                BaseAmount baseAmount = new BaseAmount();
                baseAmount.setValue(pr.getTransaction().getAmount().getBase().getValue());
                baseAmount.setCurrency(DBIConstant.CURRENCY_KEY);

                FeeAmount feeAmount = new FeeAmount();
                feeAmount.setValue(pr.getTransaction().getAmount().getFee().getValue());
                feeAmount.setCurrency(DBIConstant.CURRENCY_KEY);

                TotalAmount totalAmount = new TotalAmount();
                totalAmount.setValue(pr.getTransaction().getAmount().getTotal().getValue());
                totalAmount.setCurrency(DBIConstant.CURRENCY_KEY);

                AmountBreakdown amountBreakdown = new AmountBreakdown();
                amountBreakdown.setBase(baseAmount);
                amountBreakdown.setFee(feeAmount);
                amountBreakdown.setTotal(totalAmount);

                TransactionCallback transactionCallback = new TransactionCallback();
                transactionCallback.setReceiptNumber(pr.getId().split("-")[splitArray]);
                transactionCallback.setAmount(amountBreakdown);

                CallbackRequest callbackRequest = new CallbackRequest();
                callbackRequest.setId(pr.getId());
                callbackRequest.setResult(result);
                callbackRequest.setBiller(biller);
                callbackRequest.setTransaction(transactionCallback);

                HttpEntity<String> entity = new HttpEntity<>(new JSONObject(gson.toJson(callbackRequest)).toString(), headers);
                ResponseEntity<String> s = restTemplate.exchange( uri + pr.getId(), HttpMethod.POST, entity, String.class);
                LOGGER.info(pr.getId() + " : " + s.getStatusCode());
            }
            return Message.SUCCESSFUL;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return Message.INTERNAL_ERROR;
        }
    }
}