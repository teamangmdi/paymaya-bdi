package com.paymaya.dbi.maynilad.dao;

import lombok.Data;

@Data
public class Fields {

    private String RAP;
    private String slug;
}