package com.paymaya.dbi.maynilad;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.ScheduledEvent;
import com.paymaya.dbi.maynilad.config.DBIAccessConfig;
import com.paymaya.dbi.maynilad.config.DBIConfig;
import com.paymaya.dbi.maynilad.dao.DBIResponse;
import com.paymaya.dbi.maynilad.dao.Result;
import com.paymaya.dbi.maynilad.helper.Message;
import com.paymaya.dbi.maynilad.service.MayniladService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.HttpStatus;

/**
 * Request Handler for the Generate
 * transaction for the lambda function
 *
 * @author Mark Lawrence Saballe
 * @since 9/11/2018
 */
public class DBIGenerateHandler implements RequestHandler<ScheduledEvent, DBIResponse> {

    private static final Logger LOGGER = LoggerFactory.getLogger(DBIGenerateHandler.class);
    private DBIResponse response = new DBIResponse();
    private ApplicationContext context;
    @Autowired
    private MayniladService service;

    static AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(DBIAccessConfig.class, DBIConfig.class);
    public DBIGenerateHandler(){
        ctx.getAutowireCapableBeanFactory().autowireBean(this);
    }

    /**
     * Handle requests for Generate Transaction
     *
     * @param event This the schedule event from lambda function
     * @param context AnnotationConfigApplicationContext from DBIConfig
     * @return response object of body and status code
     */
    @Override
    public DBIResponse handleRequest(ScheduledEvent event, Context context) {
        try {
            LOGGER.info("Request received from handler. {}", event);
            response = service.generate(event.getTime());
        } catch (Exception e) {
            LOGGER.error("Error encountered while executing process for Maynilad Controller. []");
            LOGGER.error(e.getMessage());
            response.setStatusCode(HttpStatus.BAD_REQUEST.toString());
            response.setBody(new Result(Message.INTERNAL_ERROR.getCode()).toJSONValidate().toString());
        }finally {
            LOGGER.info("Response from service. {}", response);
            return response;
        }
    }
}
