package com.paymaya.dbi.maynilad.dao;

import lombok.Data;

import java.util.Map;

/**
 * Transaction DAO
 *
 * @author Mark Lawrence Saballe
 * @since 2018-06-25
 */

@Data
public class Transaction {

    private String date;
    private AmountBreakdown amount;

}