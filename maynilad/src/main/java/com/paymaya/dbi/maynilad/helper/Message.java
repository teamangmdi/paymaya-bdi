package com.paymaya.dbi.maynilad.helper;

/**
 * Message for PayMaya BDI
 *
 * @author Mark Lawrence Saballe
 * @since 6/25/2018
 */
public enum Message {

    //GENERAL
    SUCCESSFUL("0000", "Successful."),
    TRANSACTION_SUCCESSFUL("0001", "Transaction successful."),
    BAD_TRANSACTION_CODE("9001", "The Transaction code is invalid."),
    INTERNAL_ERROR("9002", "An internal error occurred."),

    //ACCOUNT NUMBER VALIDATION
    INVALID_LENGTH("2559", "Account number must be 8 characters."),
    INVALID_ACCOUNT_NUMBER("2559", "Account number is invalid."),

    NO_RECORD_FOUND("9003", "No record found."),
    METHOD_NOT_READY("9004", "Method is under development.");

    private String code;
    private String message;

    Message(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFullMessage() {
        return code + " | " + message;
    }
}