package com.paymaya.dbi.maynilad.helper;

/**
 * Message for PayMaya BDI
 *
 * @author Mark Lawrence Saballe
 * @since 2018-06-25
 */
public enum ProcessCode {

    //TRANSACTION CODE
    VALIDATE("1000", "Validate Transaction"),
    POST("2000", "Post Transaction"),
    REPORT("3000", "Report Transaction");

    private String code;
    private String detail;

    ProcessCode(String code, String message) {
        this.code = code;
        this.detail = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getFullMessage() {
        return code + " | " + detail;
    }
}