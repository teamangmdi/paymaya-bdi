package com.paymaya.dbi.maynilad.helper;

import java.math.BigDecimal;

/**
 * DBIConstant values
 *
 * @author dondon.martillano
 * @since 9/28/2018
 */

public class DBIConstant {
    //Maynilad FeeAmount Value
    public static final BigDecimal feeValue = new BigDecimal("0");

    //Payment file Details Constants
    public static final String CATEGORY_CODE_HEADER = "1";
    public static final String CATEGORY_CODE_DETAIL = "2";
    public static final String TABLE_NAME_HEADER = "BFKKZK";
    public static final String TABLE_NAME_DETAIL = "BFKKZP";
    public static final char SELECTION_CATEGORY = 'K';
    public static final String BANK_CLEARING_ACCOUNT = "9100700000";
    public static final String COMPANY_CODE = "MWSI";
    public static final String CURRENCY_KEY = "PHP";
    public static final String CLEARING_REASON = "01";

    // Validate Account Number Constants
    public static final int START = 0;
    public static final int ONE = 1;
    public static final int LENGTH = 8;
    public static final int FIRST_SEVEN = 7;
    public static final int MULTIPLIER = 2;
    public static final int HIGHEST_MULTIPLE = 10;

}
