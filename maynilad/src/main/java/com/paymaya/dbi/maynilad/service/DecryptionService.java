package com.paymaya.dbi.maynilad.service;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Service that decrypts password
 *
 */
@Component
public class DecryptionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DecryptionService.class);
    private static final String UNICODE_FORMAT = "UTF-8";
    private static final String CIPHER_TYPE = "AES/CBC/PKCS5PADDING";

    @Value("${encryption.key}")
    private String encryptionKey;

    @Value("${encryption.scheme}")
    private String encryptionScheme;

    @Value("${encryption.init.vector}")
    private String initVector;

    private Cipher cipherDecrypt;
    private SecretKeySpec secretKeySpec;

    @PostConstruct
    public void setup() throws Exception{
        IvParameterSpec iv = new IvParameterSpec(initVector.getBytes(UNICODE_FORMAT));
        secretKeySpec = new SecretKeySpec(encryptionKey.getBytes(UNICODE_FORMAT), encryptionScheme);
        cipherDecrypt = Cipher.getInstance(CIPHER_TYPE);
        cipherDecrypt.init(Cipher.DECRYPT_MODE, secretKeySpec, iv);
    }

    /**
     *  Decrypts password using AES scheme
     * @param encryptedCredential
     * @return
     */
    public String decrypt(String encryptedCredential){
        try {
            byte[] encryptedText = Base64.decodeBase64(encryptedCredential);
            byte[] plainText = cipherDecrypt.doFinal(encryptedText);
            LOGGER.info("Decryption of credential successful.");
            return new String(plainText);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(),e);
            throw new RuntimeException("Unable to decrypting credential. Returning null.",e);
        }
    }
}