package com.paymaya.dbi.maynilad.dao;

import lombok.Data;


@Data
public class BillerValidate {

    private String accountNumber;
    private String slug;
    private Fields fields;
    private Details details;
}
