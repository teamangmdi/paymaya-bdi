package com.paymaya.dbi.maynilad.api;

import com.paymaya.dbi.maynilad.dao.DBIResponse;
import com.paymaya.dbi.maynilad.dao.PostRequest;
import com.paymaya.dbi.maynilad.dao.ValidateRequest;
import org.joda.time.DateTime;

/**
 * Interface of Validate, Post and
 * Generate Transaction
 *
 * @author Mark Lawrence Saballe
 * @since 7/24/2018
 */

public interface Services {

    DBIResponse validate(ValidateRequest payload) throws Exception;

    DBIResponse postTransaction(PostRequest payload) throws Exception;

    DBIResponse generate(DateTime date) throws Exception;

}
