package com.paymaya.dbi.maynilad.dao;

import lombok.Data;

import java.util.Map;

/**
 * Biller DAO
 *
 * @author Mark Lawrence Saballe
 * @since 6/25/2018
 */


@Data
public class Biller {

    private String accountNumber;
    private Fields fields;

}
