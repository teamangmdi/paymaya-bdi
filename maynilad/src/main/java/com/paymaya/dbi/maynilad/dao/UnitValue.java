package com.paymaya.dbi.maynilad.dao;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class UnitValue {

    private String currency;
    private BigDecimal value;
}