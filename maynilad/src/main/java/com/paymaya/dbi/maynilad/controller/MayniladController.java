package com.paymaya.dbi.maynilad.controller;


import com.google.gson.Gson;
import com.paymaya.dbi.maynilad.dao.*;
import com.paymaya.dbi.maynilad.helper.Message;
import com.paymaya.dbi.maynilad.service.MayniladService;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * Maynilad Controller
 * The execute program implements an application that
 * Validate the account number, Post the transaction and
 * Generate payment file
 *
 * @author Mark Lawrence Saballe
 * @since 7/24/2018
 */

@Component
public class MayniladController {
    /**
     * This is the main method to determine whether to validate, post or generate
     *
     * @param request This is the request that will execute by this method from other service
     * @return service This returns the service depending on the path from the request
     * @throws IllegalArgumentException On incorrect input arguments
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(MayniladController.class);
    private static final String GENERATE = "generate";
    private static final String VALIDATE = "validate";
    private static final String POST = "post";

    @Autowired
    private MayniladService service;
    private Gson gson = new Gson();
    private DateTime dateTime = new DateTime();

    public DBIResponse execute(DBIRequest request) {
        try {
            String requestPath = request.getPath();
            LOGGER.info("Processing Request: {}", requestPath);
            if (Objects.isNull(request.getPath())) {
                throw new IllegalArgumentException("Invalid request path '" + requestPath + "'.");
            }
            if (requestPath.endsWith(VALIDATE)) {
                LOGGER.info("Validating Transaction with details: {}", request.getBody());
                return service.validate(gson.fromJson(request.getBody(),ValidateRequest.class));
            } else if (requestPath.endsWith(POST)) {
                LOGGER.info("Posting Transaction with details: {}", request.getBody());
                return service.postTransaction(gson.fromJson(request.getBody(), PostRequest.class));
            } else if (requestPath.endsWith(GENERATE)) {
                LOGGER.info("Generating Payment File for Transaction with date of: {}", request.getBody());
                return service.generate(dateTime);
            } else {
                throw new IllegalArgumentException("Invalid request path '" + requestPath + "'.");
            }
        } catch (Exception e) {
            LOGGER.error("Error encountered when executing request on API Gateway.");
            LOGGER.error(e.getMessage());
            PostRequest returnMessage = new PostRequest();
            returnMessage.setResult(new Result(Message.INTERNAL_ERROR.getCode()));
            DBIResponse response = new DBIResponse();
            response.setStatusCode(HttpStatus.BAD_REQUEST.toString());
            response.setBody(gson.toJson(returnMessage));
            return response;
        }
    }
}