package com.paymaya.dbi.maynilad;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.gson.Gson;
import com.paymaya.dbi.maynilad.config.DBIAccessConfig;
import com.paymaya.dbi.maynilad.config.DBIConfig;
import com.paymaya.dbi.maynilad.controller.MayniladController;
import com.paymaya.dbi.maynilad.dao.DBIRequest;
import com.paymaya.dbi.maynilad.dao.DBIResponse;
import com.paymaya.dbi.maynilad.dao.Result;
import com.paymaya.dbi.maynilad.helper.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.HttpStatus;

/**
 * Request Handler for Validate and Post
 * transaction for lambda function
 *
 * @author Mark Lawrence Saballe
 * @since 7/24/2018
 */

public class DBIHandler implements RequestHandler<DBIRequest, DBIResponse> {

    private static final Logger LOGGER = LoggerFactory.getLogger(DBIHandler.class);
    private ApplicationContext context;
    private Gson gson = new Gson();
    private DBIResponse response;
    @Autowired
    private MayniladController controller;

    static AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(DBIAccessConfig.class, DBIConfig.class);
    public DBIHandler(){
        ctx.getAutowireCapableBeanFactory().autowireBean(this);
    }

    /**
     * Handle requests for Validate and Post
     * Transaction
     *
     * @param request This is the details of the request
     * @param context AnnotationConfigApplicationContext from DBIConfig
     * @return response object of body and status code
     */
    @Override
    public DBIResponse handleRequest(DBIRequest request, Context context) {
        try {
            LOGGER.info("Request received from handler. {}", gson.toJson(request));
            response = controller.execute(request);
        } catch (Exception e) {
            LOGGER.info("Error encountered while executing process for Maynilad Controller. []", e.getMessage());
            response.setStatusCode(HttpStatus.BAD_REQUEST.toString());
            response.setBody(new Result(Message.INTERNAL_ERROR.getCode()).toJSONValidate().toString());
        }finally {
            LOGGER.info("Response from service. {}", response);
            return response;
        }
    }
}