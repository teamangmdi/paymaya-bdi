package com.paymaya.dbi.maynilad.dao;

import lombok.Data;

@Data
public class Callback {

    private String url;
    private String method;
}
