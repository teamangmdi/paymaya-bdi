package com.paymaya.dbi.maynilad.config;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.*;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.paymaya.dbi.maynilad.service.CallbackService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Configuration class for DynamoDB
 * and S3 access
 *
 * @author dondon.martillano
 * @since 10/03/2018
 */
@Configuration
public class DBIAccessConfig {

    private ClientConfiguration clientConfig;
    private AWSCredentialsProviderChain credentialsProvider;
    private AmazonDynamoDB amazonDynamoDB;
    private AmazonS3 client;

    @Value("${proxy.host}")
    private String proxyHost;

    @Value("${proxy.port}")
    private int proxyPort;

    @Value("${enableproxy}")
    private boolean enableProxy;

    @Value("${aws.signing.region}")
    private String region;

    @Value("${dynamodb.service.endpoint}")
    private String endPoint;


    /**
     * Access configuration for DynamoDB
     * @return
     */
    @Bean
    public AmazonDynamoDB amazonDynamoDB() {
        try {
            clientConfig = new ClientConfiguration();
            if (enableProxy){
                clientConfig.setProxyHost(proxyHost);
                clientConfig.setProxyPort(proxyPort);
            }
            clientConfig.setProxyProtocol(Protocol.HTTP);
        } catch (Exception e){
            throw new RuntimeException("Error loading credentials", e);
        }
        credentialsProvider = new DefaultAWSCredentialsProviderChain();
        amazonDynamoDB = AmazonDynamoDBClientBuilder.standard()
                .withCredentials(credentialsProvider)
                .withClientConfiguration(clientConfig)
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(endPoint, region)).build();
        return amazonDynamoDB;
    }

    /**
     * Access configuration for S3
     * @return
     */
    @Bean
    public AmazonS3 amazonS3(){
        try {
            clientConfig = new ClientConfiguration().withProxyProtocol(Protocol.HTTP);
            if (enableProxy){
                clientConfig.setProxyHost(proxyHost);
                clientConfig.setProxyPort(proxyPort);
            }
            clientConfig.setProxyProtocol(Protocol.HTTP);
            credentialsProvider = new DefaultAWSCredentialsProviderChain();
            client = AmazonS3ClientBuilder.standard()
                    .withRegion(region)
                    .withClientConfiguration(clientConfig)
                    .withCredentials(credentialsProvider)
                    .build();
        } catch (Exception e) {
            throw new RuntimeException("Error loading credentials", e);
        }
        return client;
    }
}