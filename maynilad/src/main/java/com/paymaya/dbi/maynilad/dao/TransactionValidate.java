package com.paymaya.dbi.maynilad.dao;

import lombok.Data;

@Data
public class TransactionValidate {

    private String date;
    private AmountBreakdown amount;
}
