package com.paymaya.dbi.maynilad.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

@ComponentScan(basePackages = {"com.paymaya.dbi.maynilad"})
@PropertySource(value = "classpath:application.properties", ignoreResourceNotFound = true)
@EnableConfigurationProperties
@EnableAutoConfiguration
//@SpringBootApplication
public class DBIConfig {

}