package com.paymaya.dbi.maynilad.dao;

import com.paymaya.dbi.maynilad.helper.Message;
import lombok.Data;
import org.json.JSONObject;

/**
 * Validate Results
 *
 * @author Mark Lawrence Saballe
 * @since 2018-07-24
 */

@Data
public class Result {

    private String code;
    private String message;

    public Result() {
    }

    public Result(String flag) {
        switch (flag) {
            case "0000":
                this.code = Message.SUCCESSFUL.getCode();
                this.message = Message.SUCCESSFUL.getMessage();
                break;
            case "2559":
                this.code = Message.INVALID_ACCOUNT_NUMBER.getCode();
                this.message = Message.INVALID_ACCOUNT_NUMBER.getMessage();
                break;
            default:
                this.code = Message.INTERNAL_ERROR.getCode();
                this.message = Message.INTERNAL_ERROR.getMessage();
                break;
        }
    }

    public JSONObject toJSON() {
        JSONObject jo = new JSONObject();
        jo.put("code", this.code);
        jo.put("message", this.message);
        return jo;
    }

    public JSONObject toJSONValidate() {
        JSONObject j1 = new JSONObject();
        j1.put("code", this.code);
        j1.put("message", this.message);
        JSONObject j2 = new JSONObject();
        j2.put("result", j1);
        return j2;
    }
}
