package com.paymaya.dbi.maynilad.dao;

import lombok.Data;

@Data
public class Details {

    private String RAP;
    private String slug;
}
