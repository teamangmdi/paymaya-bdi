package com.paymaya.dbi.maynilad;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MayniladValidateServiceTest {

    private List<String> CANS = Arrays
            .asList("54922930", "60416333", "53952007", "63925288", "63134702", "64586801",
                    "63291104", "58574460", "50428063", "50392555", "63082982", "1234567", "12345678");

    private void validateProcess(String CAN) {
        int sum = 0, n;
        if (CAN.length() != 8) {
            System.out.println(CAN + " is invalid");
        } else {
            for (int x = 0; x < 8; x++) {
                int z = Character.getNumericValue(CAN.charAt(x));
                if (x == 0) {
                    sum = z;
                } else if (x == 7) {
                    int a = ((sum / 10) * 10);
                    if (a != sum) {
                        a += 10;
                    }
                    if (a - sum == z) {
                        System.out.println(CAN + " is valid");
                    } else {
                        System.out.println(CAN + " is invalid");
                    }
                } else {
                    if (x % 2 == 1) {
                        z *= 2;
                        if ((int) (Math.log10(z) + 1) == 2) {
                            n = 0;
                            while (z > 0) {
                                n += z % 10;
                                z /= 10;
                            }
                            sum += n;
                        }
                    }
                    sum += z;
                }
            }
        }
    }

    @Test
    public void checkDigit() {
        CANS.forEach(d -> {
            System.out.println(d);
            validateProcess(d);
        });
    }

}
