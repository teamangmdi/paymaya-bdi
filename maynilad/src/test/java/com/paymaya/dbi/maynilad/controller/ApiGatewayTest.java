package com.paymaya.dbi.maynilad.controller;

import com.google.gson.Gson;
import com.paymaya.dbi.maynilad.config.DBIConfig;
import com.paymaya.dbi.maynilad.config.DBIAccessConfig;
import com.paymaya.dbi.maynilad.dao.DBIRequest;
import com.paymaya.dbi.maynilad.dao.DBIResponse;
import com.paymaya.dbi.maynilad.repo.MayniladRepository;
import com.paymaya.dbi.maynilad.service.MayniladService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

//@ComponentScan("com.paymaya.dbi.maynilad")
@RunWith(SpringRunner.class)
//@SpringBootTest(classes = {ApiGatewayTest.class, DBIConfig.class, DynamoDBConfig.class, MayniladService.class, MayniladCrudRepository.class})
@SpringBootTest(classes = {DBIConfig.class, MayniladService.class, MayniladRepository.class, DBIAccessConfig.class})
@TestPropertySource("classpath:application.properties")
public class ApiGatewayTest {

    @Autowired
    private MayniladController gate;

    @Autowired
    private MayniladService service;
    private DBIRequest request = new DBIRequest();
    private Gson gson = new Gson();

    @Before
    public void request() {
        String testpath = "https://bldj04s8c0.execute-api.ap-southeast-1.amazonaws.com/Dev/generate";
        String generatePayload = "{\n" +
                "\t\"test\":\"test payload\"\n" +
                "}";
        String validatePayload = "{\n" +
                "    \"id\": \"40f70c66-fdfa-432b-8ada-2411f02715b6\",\n" +
                "    \"biller\": {\n" +
                "        \"accountNumber\": \"6392528\",\n" +
                "        \"slug\": \"MAYNILADD\",\n" +
                "        \"fields\": {\n" +
                "            \"RAP\": \"true\"\n" +
                "        }\n" +
                "    },\n" +
                "    \"transaction\": {\n" +
                "        \"amount\": {\n" +
                "            \"currency\": \"PHP\",\n" +
                "            \"value\": 100\n" +
                "        },\n" +
                "        \"date\": \"2016-07-22T08:04:15.055Z\"\n" +
                "    }\n" +
                "}";

        String testbody = "{\n" +
                "    \"id\": \"5e67d842-9638-4697-a61c-cbdeb7d14369\",\n" +
                "    \"biller\": {\n" +
                "        \"accountNumber\": \"30655639\",\n" +
                "        \"fields\": {\n" +
                "            \"firstName\": \"Ddddd\",\n" +
                "            \"lastName\": \"Butterfield\",\n" +
                "            \"contactNumber\": \"+639384618830\"\n" +
                "        }\n" +
                "    },\n" +
                "    \"transaction\": {\n" +
                "        \"amount\": {\n" +
                "            \"total\": {\n" +
                "                \"currency\": \"PHP\",\n" +
                "                \"value\": 110\n" +
                "            },\n" +
                "            \"base\": {\n" +
                "                \"currency\": \"PHP\",\n" +
                "                \"value\": 100\n" +
                "            },\n" +
                "            \"fee\": {\n" +
                "                \"currency\": \"PHP\",\n" +
                "                \"value\": 10\n" +
                "            }\n" +
                "        }\n" +
                "    },\n" +
                "    \"callback\": {\n" +
                "        \"url\": \"http://billspay-callback.com/{id}\",\n" +
                "        \"method\": \"POST\"\n" +
                "    }\n" +
                "}";

        request.setPath(testpath);
        request.setBody(generatePayload);
    }

    @Test
    public void execute() {
        DBIResponse response = gate.execute(request);
        System.out.println(response);
    }

    @Test
    @Ignore
    public void mayniladServiceTest() {
        //System.out.println(service.validate(gson.fromJson(request.getBody(), ValidateRequest.class)));
    }
}